# Логи выполнения ansible-playbook  
```
$ ansible-playbook l2_1.2.yml -i inventories/file/hosts -vv
ansible-playbook 2.9.6
  config file = /etc/ansible/ansible.cfg
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3/dist-packages/ansible
  executable location = /usr/bin/ansible-playbook
  python version = 3.8.10 (default, Nov 26 2021, 20:14:08) [GCC 9.3.0]
Using /etc/ansible/ansible.cfg as config file

PLAYBOOK: l2_1.2.yml ***********************************************************************************************************************************
1 plays in l2_1.2.yml

PLAY [Lesson 2, Task 1.2] ******************************************************************************************************************************

TASK [Gathering Facts] *********************************************************************************************************************************
task path: /home/gb/ansible_2022-07/gb_scm/L2/l2_1.2.yml:2
ok: [127.0.0.1]
META: ran handlers

TASK [create_file : Create dir "/tmp/L2_1.2_DIR"] ******************************************************************************************************
task path: /home/gb/ansible_2022-07/gb_scm/L2/roles/create_file/tasks/main.yml:2
changed: [127.0.0.1] => {"changed": true, "gid": 1000, "group": "user", "mode": "0775", "owner": "user", "path": "/tmp/L2_1.2_DIR", "size": 4096, "state": "directory", "uid": 1000}

TASK [create_file : Create file "/tmp/L2_1.2_DIR/L2_1.2_FILE"] *****************************************************************************************
task path: /home/gb/ansible_2022-07/gb_scm/L2/roles/create_file/tasks/main.yml:6
changed: [127.0.0.1] => {"changed": true, "dest": "/tmp/L2_1.2_DIR/L2_1.2_FILE", "gid": 1000, "group": "user", "mode": "0664", "owner": "user", "size": 0, "state": "file", "uid": 1000}
META: ran handlers
META: ran handlers

PLAY RECAP *********************************************************************************************************************************************
127.0.0.1                  : ok=3    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0 
```
# Проверка наличия директории и файла  
```
$ ls -l /tmp/L2_1.2_DIR/
total 0
-rw-rw-r-- 1 user user 0 июл  2 05:29 L2_1.2_FILE
```