#### Системы управления конфигурациями, 2022-06

## Урок 1
### Практическое задание
> Сравнить все системы между собой, показав плюсы и минусы.
> Ожидаемый результат — таблица, в которую необходимо включить название системы, сильные и
слабые стороны. По желанию вы также можете добавить выбранные вами системы, не рассматриваемые в этом уроке.  

  
| SCM | Тип | Агент | Протокол | "+" | "-" |
| --- | --- | --- | --- | --- | --- |
| Ansible   | Push | - | SSH | - Прост в настройке<br> - Не требует установки агентов<br> - Самое частое решение для небольших инфраструктур | - Не удобен при большом количестве хостов<br> - Работа с ОС Windows только через PowerShell<br> - Транспорт SSH снижает скорость<br> - Необходим контроль SSH ключей | 
| Chef   | Pull/Push | + | HTTPS, SSH | - Можно использовать язык Ruby для создания рецептов<br> - Имеет отдельный инструмент управления инфраструктурой (Workstation)<br> -  | - Push только в Enterprise версии<br> -  |
| Puppet   | Pull/Push | + | Mcollective, HTTPS | - Есть GUI | - Ruby-подобный язык для создания манифестов<br> |
| SaltStack | Pull/Push | +/- | ZeroMQ | - Имеет интеграцию с большими провайдерами и датацентрами<br> - Расширяемость<br> - Высокая скорость<br> - Работа по событиями/триггерам<br> - Широкий спект поддерживаемых устройств | - Ядро развивается медленно<br> |
| CFengine | Pull | + | Свой протокол на TLS | - Описание состояния инфраструктуры на<br> основе "теории обещаний (контрактов)"<br> - Низкий уровень потребления ресурсов  | -  Сложен в изучении<br> - Есть ограничения при использовании на различных ОС<br> - Есть бесплатная версия |

___

#### Ссылки:  
- https://cyberleninka.ru/article/n/sravnitelnyy-obzor-sredstv-upravleniya-konfiguratsiyami-resursov-vychislitelnoy-sredy-funktsionirovaniya-tsifrovyh-dvoynikov/viewer  
- https://en.wikipedia.org/wiki/Comparison_of_open-source_configuration_management_software  
- https://gbcdn.mrgcdn.ru/uploads/asset/3847787/attachment/3bca712e71d8cb58dbfabd642e602276.pdf  
- https://docs.chef.io/platform_overview/  
- https://docs.cfengine.com/docs/3.17/reference.html  
- https://puppet.com/docs/  
- https://docs.ansible.com/ansible/latest/index.html  
- https://cyberleninka.ru/article/n/avtomatizirovannaya-ustanovka-i-konfigurirovanie-servernyh-resheniy/viewer  
- https://www.upguard.com/blog/puppet-vs-cfengine  
- https://www.opstrainerz.com/blog/chef-vs-puppet  
- https://medium.com/@anthonypjshaw/ansible-v-s-salt-saltstack-v-s-stackstorm-3d8f57149368  

