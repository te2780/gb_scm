#!/usr/bin/env bash

#apt install -y yamllint ansible-lint
# Yamllint check
yamllint -c .yamllint --no-warnings .
# Ansible-lint check
ansible-lint -c .ansible-lint --exclude=.git .