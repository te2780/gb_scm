Links:
1) https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html
2) https://docs.ansible.com/ansible/latest/network/getting_started/first_inventory.html

Tasks:
1) Добавить .yamlling, .ansible-lint, написать скрипт запуска валидаторов  
2) В роль конфигурирования nginx добавить assert (проверить, что все параметры заданы верно)  
3*) Настроить в своем gitlab-репозитории gitlab-ci

