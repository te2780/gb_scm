Links:
1) https://molecule.readthedocs.io/en/latest/
2) https://www.ansible.com/hubfs//AnsibleFest%20ATL%20Slide%20Decks/Practical%20Ansible%20Testing%20with%20Molecule.pdf
3) https://www.ansible.com/blog/developing-and-testing-ansible-roles-with-molecule-and-podman-part-1
4) https://molecule.readthedocs.io/en/latest/getting-started.html

Tasks:
1) Создать новую роль с помощью molecule с нуля, которая будет устанавливать
на удаленную машину docker_container с redis.
Для verify проверять: порт с redis прослушивается, docker_container запущен

-------------
Описание скриптов в roles/docker/README.md 