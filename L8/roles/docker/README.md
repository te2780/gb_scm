# How to use
------------

## Создание docker контейнера
------------------
```
molecule create
```

Создается контейнер с именем redis-server-docker с параметрами:
```
  - name: redis-server-docker
    image: redis:latest
    pre_build_image: true
    command: redis-server
    published_ports:
    - "0.0.0.0:6379:6379/tcp"
    volumes:
    - /var/run/docker.sock:/var/run/docker.sock
```
Проброс volumes необходим для использования модуля docker_container_info в verify.  
После создания в контейнер нет python, поэтому при помощи модуля raw в скрипте prepare устанавливаются необходимые пакеты:  
python3 python3-requests python3-docker  

Они нужны для запуска verify (внутри контейнера)

## Запуск verify
```
molecule verify
```
Сначала при помощи модуля docker_container_info собирается информация о контейнере.
Потом при помощи модуля command запускается команда:
```
redis-cli -h localhost -p 6379 ping
```
которая должа выдать ответ PONG.