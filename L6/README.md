### Links:
1) https://jinja.palletsprojects.com/en/3.1.x/

### Tasks:
1) Написать роль конфигурирования nginx  
1.1) Создать конфиг на jinja2 templates/nginx.conf.j2  
1.2) В файле (templates/nginx.conf.j2) шаблонизировать nginx_port, worker_connections
1.3) В templates/nginx.conf.j2 Реализовать шаблонизацию списка апстримов из хостов группы инвентори
2) Написать роль, которая скачает статичный файл (например, https://github.com/ansible/ansible/blob/stable-2.7/examples/ansible.cfg), проверит его по чексумме
3) Написать роль, которая на удаленный сервер перенесет файл (предварительно создать такой файл в <role>/files/<file>)
